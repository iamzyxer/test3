# Задача #

Реализовать сервис подсказок адресов

Frontend:

    Форма из одного input

    Подсказки начинаются после ввода 3-его символа

    Максимум 6 подсказок

    После выбора подсказки - весь текст подставляется в форму

    Запрос на ваш backend

    Без jquery

    react/vue/angular + webpack


Backend:

    Реализовать один метод api - возвращающий подсказки

    В этом методе для получения подсказок используйте сервис от яндекса или гугла

    Все запросы в геосервис и их результаты логировать в базе (на ваш выбор)

    Можно использовать сторонние компоненты, либы, фреймворки, все что захотите


Важным фактором оценки задания будет:

    Возможность выкатить этот сервис в продакшен, насколько он будет готов к нему

    Напишите небольшой текст, в котором опишите, как бы вы стали масштабировать свой сервис на 100к запросов в секунду

    Если у вас есть такая возможность, поднимите этот сервис где-то у себя


Результат: Ссылка на открытый репозиторий, доступы от сервера (если есть), описание масштабирования


#Структура#

task - директория содержит файлы задания

index.php - точка входа

Dump:

```mysql
CREATE TABLE IF NOT EXISTS `log` (
`id` int(11) unsigned NOT NULL,
  `request` varchar(255) NOT NULL,
  `response` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

ALTER TABLE `log` ADD PRIMARY KEY (`id`);
ALTER TABLE `log` MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
```

# Установка #

1. git clone https://iamzyxer@bitbucket.org/iamzyxer/test3.git
2. composet install
3. npm install && npm run build
4. Создать БД
5. Создать конфиг cp config/application.dist.php config/application.local.php
6. Настроить в конфиге соединение с БД