<?php defined('ROOT') or die('No direct access allowed');

return [
    'db' => [
        'host'     => '127.0.0.1',
        'user'     => 'db_user',
        'password' => 'password',
        'dbname'   => 'db_test',
        'charset'  => 'UTF8',
    ],
    'application' => [
        'path' => null, //demo
        'default' => 'index', //index
    ]
];