<?php

define('DS',   DIRECTORY_SEPARATOR);
define('ROOT', __DIR__);

require_once ROOT . '/vendor/autoload.php';

class Controller
{
    protected $path;
    public $cmd;
    protected $flock;
    protected $config;
    /** @var \PDO  */
    protected $pdo;
    protected $ver = 'bnrk38';

    protected $map = [
        'index' => 'indexAction',
        'search' => 'searchAction',
        'history' => 'historyAction',
    ];

    public function __construct()
    {
        $this->config = $config = $this->getConfig();
        $this->path = $config['application']['path'];

        $this->before();

        if ($this->isCli()) {
            $this->cmd = $this->getCmdForCli();
        } else {
            $this->cmd = $this->getCmdForRequest();
        }
    }

    public function before()
    {
        $config = $this->config['db'];

        $dsn = "mysql:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            $this->pdo = new PDO($dsn, $config['user'], $config['password'], $opt);
        } catch (PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|mixed|null
     */
    public function getConfig($key = null, $default = null)
    {
        if ($this->config === null) {
            if (is_readable($path = ROOT . DS . 'config' . DS . 'application.local.php')) {
                $this->config = include_once $path;
            } elseif (is_readable($path = ROOT . DS . 'config' . DS . 'application.global.php')) {
                $this->config = include_once $path;
            } else {
                $this->config = [];
            }
        }

        if ($key === null) {
            return $this->config;
        } else {
            if (isset($this->config[$key])) {
                return $this->config[$key];
            }
        }

        return $default;
    }

    public function isCli()
    {
        return PHP_SAPI === 'cli';
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getCmdForRequest()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $request = preg_split('~\?~', $_SERVER['REQUEST_URI']);
            $request = $request[0];
        } elseif (!empty($_SERVER['PATH_INFO'])) {
            $request = $_SERVER['PATH_INFO'];
        } elseif (isset($_SERVER['PHP_SELF'])) {
            $request = str_replace('index.php', '', $_SERVER['PHP_SELF']);
        } else {
            throw new \Exception('Unable to detect the URI using REQUEST_URI, PATH_INFO, PHP_SELF');
        }

        if (!empty($this->path)) {
            $request = preg_replace('~^' . preg_quote($this->path) . '~i', '', $request);
        }

        return trim($request, '/');
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getCmdForCli()
    {
        if (empty($_SERVER['argv'])) {
            throw new \Exception('Controller not found');
        }

        $arguments = array();

        foreach ($_SERVER['argv'] as $arg) {
            if (strpos($arg, '=') !== false) {
                $_arg = explode('=', $arg);

                $arguments[$_arg[0]] = $_arg[1];
            }
        }

        return empty($arguments['cmd']) ? null : $arguments['cmd'];
    }

    /**
     * @return string
     */
    public function getVer()
    {
        return $this->ver;
    }

    /**
     * @param null $cmd
     * @param string $default
     */
    public function forward($cmd = null, $default = 'index')
    {
        $cmd = empty($cmd) ? $this->cmd : $cmd;
        if (empty($cmd)) {
            $this->cmd = $cmd = $default;
        }

        if (empty($this->map[$cmd])) {
            if ($this->isCli()) {
                print 'Command not found';
                exit();
            }
            $this->notFoundAction();
        }

        $method = $this->map[$cmd];
        if ($this->isCli()) {
            $this->$method();
            exit();
        }

        header(sprintf('Content-type: %s; charset=%s', 'text/html', 'utf-8'));
        print $this->$method();
        exit();
    }

    protected function notFoundAction()
    {
        header((empty($_SERVER['SERVER_PROTOCOL']) ? 'HTTP/1.1' : $_SERVER['SERVER_PROTOCOL']) . ' 404 Not Found', true, 404);
        include 'phtml/404.phtml';
        exit();
    }

    /**
     * @return string
     */
    protected function indexAction()
    {
        include 'dist/index.html';
    }

    /**
     * @return string
     */
    protected function searchAction()
    {
        $data = [
            'items' => [],
            'points' => [],
        ];

        $points = $this->getGeopoints(empty($_GET['keyword']) ? null : $_GET['keyword']);
        foreach ($points as $point) {
            $data['items'][] = $point['address'];
        }

        $data['points'] = $points;

        header('Expires: 0');
        header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        header('Content-type: application/json');

        print json_encode($data);
        exit();
    }

    /**
     * @param $geocode
     * @return array
     */
    protected function getGeopoints($geocode)
    {
        $points = [];
        if (empty($geocode)) return $points;

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', "http://geocode-maps.yandex.ru/1.x/?format=json&geocode={$geocode}");

        if ($res->getStatusCode() != 200) {
            return $points;
        }


        $_contents = $res->getBody()->getContents();
        $contents = @json_decode($_contents);
        if (empty($contents)) return $points;

        if ($contents->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
            //echo $contents->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
            foreach ($contents->response->GeoObjectCollection->featureMember as $i) {
                $points[] = [
                    'address' => $i->GeoObject->description . ' ' . $i->GeoObject->name,
                    'point' => explode(' ', $i->GeoObject->Point->pos)
                ];
            }
        }

        $sql = "INSERT INTO `log`
                    SET
                    `request` = :request,
                    `response` = :response,
                    `created` = NOW(),
                    `updated` = NOW()";

        $stm = $this->pdo->prepare($sql);
        $stm->execute([
            'request' => $geocode,
            'response' => $_contents,
        ]);

        return $points;
    }

    /**
     * @return mixed
     */
    public function historyAction()
    {
        $history = $this->getHistory();
        header('Expires: 0');
        header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        header('Content-type: application/json');

        print json_encode([
            'items' => $history,
        ]);
        exit();
    }

    protected function getHistory()
    {
        $stmt = $this->pdo->prepare('SELECT `id`, `request`, `created` FROM `log` ORDER BY `created` DESC LIMIT 30');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @param $path
     * @return string
     */
    public function uri($path)
    {
        return $this->path . $path;
    }

    public function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
    {
        if ($length == 0) return '';

        if (mb_strlen($string, 'UTF-8') > $length) {
            $length -= min($length, mb_strlen($etc, 'UTF-8'));
            if (!$break_words && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length + 1, 'UTF-8'));
            }
            if (!$middle) {
                return mb_substr($string, 0, $length, 'UTF-8') . $etc;
            }

            return mb_substr($string, 0, $length / 2, 'UTF-8') . $etc . mb_substr($string, - $length / 2, $length, 'UTF-8');
        }
        return $string;
    }
}


$controller = new Controller();
$config = $controller->getConfig('application');
$controller->forward(null, $config['default']);